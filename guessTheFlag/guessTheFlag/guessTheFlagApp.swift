//
//  guessTheFlagApp.swift
//  guessTheFlag
//
//  Created by Arnaud Chrétien on 26/09/2021.
//

import SwiftUI

@main
struct guessTheFlagApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
